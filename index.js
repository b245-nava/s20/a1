// console.log("hi")
/*
Instructions that can be provided to the students for reference:
Activity:
1. In the S20 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable number that will store the value of the number provided by the user via the prompt.
4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
5. Create a condition that if the current value is less than or equal to 50, stop the loop.
6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7. Create another condition that if the current value is divisible by 5, print the number.
*/

/*	let userNum = Number(prompt("Provide a number:"));

	do{
		if(userNum % 10 === 0){
			console.log("The number is divisible by ten. Skipping the number...");
			continue;
		}
		if(userNum % 5 === 0){
			console.log(userNum);
		}
		if(userNum <=50){
			break;
		}
	}*/

	for(let x = Number(prompt("Provide a number:")); x>=0; x--){
		if(x % 10 === 0){
			console.log("The number is divisible by ten. Skipping the number...");
			continue;
		}
		else if(x % 5 === 0){
			console.log(x);
		}
		else if(x<=50){
			break;
		}
	}



/*
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.
*/

let word = "supercalifragilisticexpialidocious";
let consonants = "";

console.log(word);

for(let i = 0; i<word.length; i++){
	if(word[i] === "a"||
		word[i] === "e"||
		word[i] === "i"||
		word[i] === "o"||
		word[i] === "u"){
		continue;
	}
		consonants += word[i];
}

console.log(consonants);

/*
13. Create a git repository named S20.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.
*/